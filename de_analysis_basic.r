########
### DE analysis - Chrys Vraka StressProject Mouse QuantSeq
### Karolina Trachtova
### DE analysis of tissue subtype 'AG'
########

# Load libraries ####-----------------------------------------------------------
library(TCGAbiolinks)
library(SummarizedExperiment)
library(openxlsx)
library(dplyr)
library(stringr)
library(KEGGREST)
library(DESeq2)
library(org.Hs.eg.db)
library(pheatmap)
library(gplots)
library(viridis)
library(RColorBrewer)
library(PCAtools)
library(clusterProfiler)
library(fgsea)
library(GSVA)
library(limma)
library(tibble)
library(data.table)
library(enrichplot)
library(DOSE)
library(EnhancedVolcano)
library(tidyr)
library(msigdbr)
library(tximport)
library(org.Mm.eg.db)

# Prepare project ####----------------------------------------------------------
project_dir <- "/mnt/nfs/home/422653/000000-My_Documents/RNA-Seq/vienna/Chrys_stressProject_MouseQuant-seq"
datadir <- "/mnt/nfs/home/422653/000000-My_Documents/RNA-Seq/vienna/Chrys_stressProject_MouseQuant-seq/data"
plotdir <- "/mnt/nfs/home/422653/000000-My_Documents/RNA-Seq/vienna/Chrys_stressProject_MouseQuant-seq/plots"
resultdir <- "/mnt/nfs/home/422653/000000-My_Documents/RNA-Seq/vienna/Chrys_stressProject_MouseQuant-seq/results"

P_THRESHOLD<-0.05
FOLD_CHANGE<-1.5
LFC_THRESHOLD<-log(FOLD_CHANGE, 2)
TOP<-10 # How many top DE genes should be plotted
INTERCEPT<-FALSE # c('TRUE', 'FALSE') - do we want to calculate with intercept - https://support.bioconductor.org/p/69341/
PAIRED<-FALSE # c('TRUE', 'FALSE') - do we have paired data? This could be data in different time points from the same patient/samples

hmcol<-colorRampPalette(brewer.pal(9, "GnBu"))(100)

pdf<-NULL
if (!(is.null(pdf))){
  pdf(pdf, paper="a4")
}

organ <- "AG"
diet <- "lean"

resultdir <- paste0("/mnt/nfs/home/422653/000000-My_Documents/RNA-Seq/vienna/Chrys_stressProject_MouseQuant-seq/results/DE_",organ,"_",diet)
dir.create(resultdir)

# Load data ####----------------------------------------------------------------

# Samples description, should minimally contain columns "sample", "name", "condition", "patient" (and additionally "run" and/or "prep" = library prep date, these are used to remove batch effect)
samples_desc<-read.table(paste0(datadir,"/design_AG_lean.txt"), header=T, stringsAsFactors = T, sep="\t")
samples_desc$sample <- as.character(samples_desc$sample)
rownames(samples_desc) <- samples_desc$sample

# Read RSEM counts
files <- list.files(paste0(datadir,"/counts/RSEM"), pattern = ".genes.results",full.names = T)
names(files) <- str_replace(basename(files),".genes.results","")
txi.rsem <- tximport(files, type = "rsem", txIn = FALSE, txOut = FALSE)

# check if all samples from design are in the count matrix -> OK
samples_desc$sample %in% colnames(txi.rsem$counts)

# remove samples that are not in the design out of the count matrices
txi.rsem$counts <- txi.rsem$counts[,colnames(txi.rsem$counts) %in% samples_desc$sample]
txi.rsem$abundance <- txi.rsem$abundance[,colnames(txi.rsem$abundance) %in% samples_desc$sample]
txi.rsem$length <- txi.rsem$length[,colnames(txi.rsem$length) %in% samples_desc$sample]

txi.rsem$counts <- txi.rsem$counts[,match(rownames(samples_desc), colnames(txi.rsem$counts))]

matrix_counts <- txi.rsem$counts

# Pre-DE quality check ####-----------------------------------------------------

# check library sizes
cond_colours<-viridis(length(unique(samples_desc$treatment)))[as.factor(samples_desc$treatment)]
names(cond_colours)<-samples_desc$treatment

pdf(file=paste0(resultdir,"/counts_barplot.pdf"), width=16, height=8)
par(mar = c(10,5,4,2) + 0.1)
bp<-barplot(apply(matrix_counts, 2, sum), col = cond_colours, las=2,, ylim=c(0,(max(apply(matrix_counts, 2, sum)))*1.1))
text(bp, apply(matrix_counts,2,sum), labels=round(apply(matrix_counts, 2, sum), 0), cex=1, pos=3) # https://stackoverflow.com/questions/27466035/adding-values-to-barplot-of-table-in-r
legend("topleft", levels((samples_desc$organ)), cex=0.6, fill=cond_colours[levels(samples_desc$organ)])
dev.off()

# sink(paste0(datadir,"/DESeq2_design_control.txt"))
# samples_desc
# sink()

# DE analysis ####--------------------------------------------------------------

#* Run DESeq2 ####--------------------------------------------------------------
# one more check that design and matrix of counts matches
rownames(samples_desc) == colnames(matrix_counts)

samples_desc$treatment <- relevel(samples_desc$treatment, ref = "control")
dds<-DESeqDataSetFromMatrix(round(matrix_counts,0), colData = samples_desc, design = ~ sex + treatment)

# Remove very low count genes
keep <- rowSums(counts(dds)) >= 50
dds <- dds[keep,]

# The same thing which follows be calculated by >DESeq(dds) instead of three separate commands
dds<-estimateSizeFactors(dds)
dds<-estimateDispersions(dds)
dds<-nbinomWaldTest(dds)
cds<-dds

#* Extract results for comparison ####------------------------------------------

#cond1 <- "chronic"
#cond2 <- "control"

cond1 <- "restraint"
cond2 <- "cooling"

resultsNames(cds)
res <- results(cds, contrast=c("treatment", cond1, cond2), independentFiltering = T, alpha = 0.05) # Extract contrasts of interest
summary(res)

dir.create(paste0(resultdir,"/",cond1,"VS",cond2))

#* Post-DE quality check  ####--------------------------------------------------
pdf(file=paste0(resultdir,"/", cond1,"VS",cond2,"/DESeq2_disperison_plot.pdf"))
plotDispEsts(cds, main=paste0("Dispersion Plot - ", organ, " ", cond1, "VS", cond2))
dev.off()

rawcounts<-counts(cds, normalized=FALSE) # Save raw counts
normcounts<-counts(cds, normalized=TRUE) # Save normalized counts
log2counts<-log2(normcounts+1) # Save log2 of normalized counts

vsd<-DESeq2::varianceStabilizingTransformation(cds) # Save counts tranformed with variance Stabilizing Transformation
vstcounts<-assay(vsd)

# PCA plots
p <- PCAtools::pca(vstcounts[,colnames(vstcounts) %in% rownames(samples_desc[samples_desc$treatment %in% c(cond1,cond2),])], metadata = samples_desc[samples_desc$treatment %in% c(cond1,cond2),])

#cl_treatment <- list(chronic = "#424086", control = "#26828e")
cl_treatment <- list(restraint = "#FDE725FF", cooling = "#35B779FF")
cl_sex <- list(female = "#E69F00", male = "#56B4E9")

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_sex.pdf"), width = 5, height = 5)
svg(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_sex.svg"), width = 5, height = 5)

print(PCAtools::biplot(p,pointSize = 5,
                 colby = "sex",
                 colkey = cl_sex,
                 gridlines.major = F, gridlines.minor = F,
                 labSize = 4,
                 legendPosition = "top",axisLabSize = 13))
dev.off()

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_treatment.pdf"), width = 5, height = 5)
svg(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_treatment.svg"), width = 5, height = 5)

print(PCAtools::biplot(p,pointSize = 5,
                       colby = "treatment",
                       colkey = cl_treatment,
                       gridlines.major = F, gridlines.minor = F,
                       labSize = 4,
                       legendPosition = "top",axisLabSize = 13))
dev.off()


mm <- model.matrix(~treatment, colData(vsd))
newCounts<-removeBatchEffect(vstcounts, vsd$sex, design = mm)

p_batch <- PCAtools::pca(newCounts[,colnames(newCounts) %in% rownames(samples_desc[samples_desc$treatment %in% c(cond1,cond2),])], metadata = samples_desc[samples_desc$treatment %in% c(cond1,cond2),])

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_treatment_sexBatchRemoved.pdf"), width = 5, height = 5)
svg(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_treatment_sexBatchRemoved.svg"), width = 5, height = 5)

print(PCAtools::biplot(p_batch,pointSize = 5,
                       colby = "treatment",
                       colkey = cl_treatment,
                       gridlines.major = F, gridlines.minor = F,
                       labSize = 4,
                       legendPosition = "top",axisLabSize = 13))
dev.off()

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_sex_sexBatchRemoved.pdf"), width = 5, height = 5)
svg(file=paste0(resultdir,"/", cond1,"VS",cond2, "/PCA_plot_sex_sexBatchRemoved.svg"), width = 5, height = 5)

print(PCAtools::biplot(p_batch,pointSize = 5,
                       colby = "sex",
                       colkey = cl_sex,
                       gridlines.major = F, gridlines.minor = F,
                       labSize = 4,
                       legendPosition = "top",axisLabSize = 13))
dev.off()

hmcol<-colorRampPalette(brewer.pal(9, "GnBu"))(100)
res

# Heatmaps - deprecated
# pdf(file=paste0(resultdir,"/", cond1,"VS",cond2,"/heatmaps_samples.pdf"))
# heatmap.2(cor(newCounts), trace="none", col=hmcol, main="Sample to Sample Correlation (VST)", RowSideColors=cond_colours, margins=c(9,7))
# heatmap.2(cor(log2counts), trace="none", col=hmcol, main="Sample to Sample Correlation (Log2)", RowSideColors=cond_colours, margins=c(9,7))
# heatmap.2(cor(rawcounts), trace="none", col=hmcol, main="Sample to Sample Correlation (Raw Counts)", RowSideColors=cond_colours, margins=c(9,7))
# dev.off()

#* Annotate genes ####----------------------------------------------------------
gtf_file <- "/mnt/ssd/ssd_3/references/mus_musculus/GRCm38.p6-93/annot/GRCm38.p6-93.gtf"

GenDB <- rtracklayer::import(gtf_file)
parsedGENCODE <- as.data.frame(GenDB[GenDB$type=="gene",])
parsedGENCODE <- parsedGENCODE[,c("gene_id", "gene_name", "gene_biotype")]
parsedGENCODE[is.na(parsedGENCODE$gene_name), "gene_name"]<-parsedGENCODE[is.na(parsedGENCODE$gene_name), "gene_id"] # Replace missing gene names by gene ids
rownames(parsedGENCODE)<-parsedGENCODE$gene_id

res2<-as.data.frame(res)

res2 <- merge(res2, parsedGENCODE, by = "row.names", all.x=T, all.y=F)
rownames(res2) <- res2$Row.names
res2$Row.names<-NULL

#* Extract DE statistics table ####---------------------------------------------

res2<-merge(res2, counts(dds, normalized=TRUE), by="row.names") # Add norm. counts
rownames(res2)<-res2$Row.names
res2$Row.names<-NULL

res2<-merge(res2, counts(dds, normalized=FALSE), by="row.names") # Add raw counts
rownames(res2)<-res2$Row.names
res2$Row.names<-NULL
res2$gene_id<-NULL

res2<-res2[with(res2, order(padj, pvalue, abs(log2FoldChange), -baseMean)), ]

colnames(res2)<-gsub(pattern = ".x", replacement = "_normCounts", fixed = T, colnames(res2))
colnames(res2)<-gsub(pattern = ".y", replacement = "_rawCounts", fixed = T, colnames(res2))

res3 <- cbind(ensembl_id = rownames(res2), res2)

write.table(x = res2, file = paste0(resultdir,"/", cond1,"VS",cond2, "/DESeq2.tsv"), sep = "\t", col.names = NA)
write.xlsx(x = res3, file = paste0(resultdir,"/", cond1,"VS",cond2, "/DESeq2.xlsx"))

#* DE plots ####----------------------------------------------------------------

# Volcanoplot for manuscript

sig_gene_name <-res2[(abs(res2$log2FoldChange) >= LFC_THRESHOLD) & (res2$padj < P_THRESHOLD) & !is.na(res2$padj),]$gene_name # Extract significant results
sig_gene_name <- sig_gene_name[1:5]

keyvals <- ifelse(
  (res2$log2FoldChange < -LFC_THRESHOLD) & (res2$padj < P_THRESHOLD) & !is.na(res2$padj), 'royalblue',
  ifelse((res2$log2FoldChange > LFC_THRESHOLD) & (res2$padj < P_THRESHOLD) & !is.na(res2$padj), 'gold',
         'black'))
keyvals[is.na(keyvals)] <- 'black'
names(keyvals)[keyvals == 'gold'] <- 'UP-regulated'
names(keyvals)[keyvals == 'black'] <- 'NS'
names(keyvals)[keyvals == 'royalblue'] <- 'DOWN-regulated'

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/EnhancedVolcano_top5.pdf", sep=""))
svg(file=paste0(resultdir,"/", cond1,"VS",cond2, "/EnhancedVolcano_top5.svg", sep=""))

print(EnhancedVolcano(res2,
                lab = res2$gene_name,
                x = 'log2FoldChange',
                y = 'padj',
                title = 'Volcano plot',
                subtitle = paste0(cond1, " VS ", cond2),
                pCutoff = 0.05,
                FCcutoff = 0.56,
                pointSize = 3.0,
                colCustom = keyvals,
                selectLab = sig_gene_name,
                drawConnectors = T,
                boxedLabels = T,
                gridlines.major = F,
                gridlines.minor = F,
                widthConnectors = 0.2,
                colConnectors = 'grey30',
                xlim = c(-2.5,2.5),
                ylim = c(0,2),
                cutoffLineWidth = NA,
                labSize = 4
))
dev.off()

# Heatmaps of selected genes

#select<-rownames(res2[(abs(res2$log2FoldChange) >= LFC_THRESHOLD) & (res2$padj < P_THRESHOLD) & !is.na(res2$padj),])
select<-rownames(res2[(abs(res2$log2FoldChange) >= LFC_THRESHOLD) & (res2$pvalue < 0.05) & !is.na(res2$padj),])
select <- select[1:20]

# Add custom genes to the heatmap
#select <- c(select, rownames(resForPlot)[resForPlot$Gene %in% c("IL2RA", "IL2RB")])

# Possible normalization methods for plotting
#nt<-DESeq2::normTransform(dds) # defaults to log2(x+1)
#nt<-DESeq2::rlog(dds, blind=FALSE)
#nt<-DESeq2::varianceStabilizingTransformation(dds, blind=FALSE)

log2.norm.counts<-newCounts[select,]
log2.norm.counts <- log2.norm.counts[,colnames(log2.norm.counts) %in% samples_desc[samples_desc$treatment %in% c(cond1, cond2),]$sample]

log2.norm.counts <- merge(log2.norm.counts, parsedGENCODE, by = "row.names", all.x=T, all.y=F)
rownames(log2.norm.counts) <- log2.norm.counts$gene_name

df<-as.data.frame(samples_desc[samples_desc$treatment %in% c(cond1, cond2),c("sample","treatment")])
rownames(df)<-df$sample # Rename rows to fit to column names
df$sample <- NULL

# Heatmap for manuscript

#cl_treatment_heatmap = list(
#  treatment = c(chronic = "#424086", control = "#26828e")
#)

cl_treatment_heatmap <- list(treatment = c(restraint = "#FDE725FF", cooling = "#35B779FF"))

# custom clustering for heatmap
mat_cluster_cols <- hclust(dist(t(log2.norm.counts)))
library(dendsort)
sort_hclust <- function(...) as.hclust(dendsort(as.dendrogram(...)))
mat_cluster_cols <- sort_hclust(mat_cluster_cols)

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/heatmap_DEGs_padj0.05_FC1.5.pdf", sep=""), width = 10, height = 7)
svg(file=paste0(resultdir,"/", cond1,"VS",cond2, "/heatmap_DEGs_padj0.05_FC1.5.svg", sep=""), width = 10, height = 7)

print(pheatmap(log2.norm.counts, 
         cluster_rows=TRUE, 
         show_rownames=T,
         cluster_cols= mat_cluster_cols, 
         annotation_col=df,
         color=magma(201),
         annotation_names_col = F,
         show_colnames=F,
         fontsize=14,
         scale="row",
         border_color = NA,cellheight = 15, cellwidth = 50,
         annotation_colors = cl_treatment_heatmap,
         treeheight_col=30, main = "Heatmap of top 20 DEGs (pvalue < 0.05 & FC > 1.5)"))
      
dev.off()

####

## Boxplots
# Boxplot using log2 normalized values
# select<-res2[(abs(res2$log2FoldChange) >= LFC_THRESHOLD) & (res2$padj < P_THRESHOLD) & !is.na(res2$padj),]$gene_name
select<-res2[(abs(res2$log2FoldChange) >= LFC_THRESHOLD) & (res2$pvalue < P_THRESHOLD) & !is.na(res2$padj),]$gene_name

tcounts <- t(log2.norm.counts) %>%
  merge(df, ., by="row.names") %>%
  gather(gene, expression, (ncol(.)-length(select)+1):ncol(.))

cond1_color <- unique(cond_colours[names(cond_colours) == cond1])
cond2_color <- unique(cond_colours[names(cond_colours) == cond2])

cond_color <- c(cond1_color, cond2_color)
names(cond_color) <- c(cond1, cond2)

pdf(file=paste0(resultdir,"/", cond1,"VS",cond2, "/top_genes_vstCounts_boxplot.pdf"), width = 5, height = 5)

for(i in select){
  tmp <- tcounts %>%
    filter(gene==i)
  print(
    ggplot(tmp, aes(treatment, expression, fill=treatment)) +
      geom_violin(alpha = 0.5)+
      geom_dotplot(binaxis='y', stackdir='center', show.legend = F, dotsize = 1.5) +
      scale_fill_manual(values=cond_color) +
      theme(panel.border = element_rect(color="black", fill = NA), 
            panel.background = element_blank(),
            panel.grid.major = element_blank(),
            text = element_text(size=15)) +
      ylab("VST counts") +
      xlab("") +
      ggtitle(i) + labs(colour="Conditions:", 
                                                          subtitle = paste0("BaseMean: ",round(res2[res2$gene_name == i,]$baseMean,2),
                                                                            paste0(" P-value: ", round(res2[res2$gene_name == i,]$pvalue,3)),
                                                                            paste0(" LogFC: ",round(res2[res2$gene_name == i,]$log2FoldChange,2))))
        
  )
}
dev.off()


# GO analyss with ClusterProfiler ####------------------------------------------

# GO over-representation test

res3 <- res2
rownames(res3) <- sub("\\..*", "", rownames(res3))
res3$gene_id <- rownames(res3)

REGUL <-'both'
for(REGUL in c("up","down","both")){
  print(paste0("Working on ",REGUL,"-regulated genes..."))
  
  SIMILARITY_CUTOFF <- 0.7
  OUTPUT_DIR <- paste0(resultdir,"/GO_ORA")
  
  dir.create(OUTPUT_DIR, recursive = T)
  
  if(REGUL == "up"){
    genes<-res3 %>%
      dplyr::filter(!is.na(padj)) %>%
      dplyr::filter(padj<0.05) %>%
      dplyr::filter(log2FoldChange >= LFC_THRESHOLD) %>%
      dplyr::select(gene_id, log2FoldChange)
    col <- "red"
  }else if(REGUL == "down"){
    genes<-res3 %>%
      dplyr::filter(!is.na(padj)) %>%
      dplyr::filter(padj<0.05) %>%
      dplyr::filter(log2FoldChange <= -LFC_THRESHOLD) %>%
      dplyr::select(gene_id, log2FoldChange)
    col <- "blue3"
  }else if(REGUL == "both"){
    genes<-res3 %>%
      dplyr::filter(!is.na(padj)) %>%
      dplyr::filter(padj<0.05) %>%
      dplyr::filter(abs(log2FoldChange) >= LFC_THRESHOLD) %>%
      dplyr::select(gene_id, log2FoldChange)
    col <- "violetred"
  }else{
    "Unknown expression change."
  }
  
  print(paste0("Number of genes to analyse: ",nrow(genes)))
  length(unique(as.character(genes$gene_id)))

  ont <- 'BP'
  for (ont in c("BP","MF","CC")){
    print(paste0("Analysing ",ont," ontology..."))
    ego <- enrichGO(gene          = genes$gene_id,
                    universe      = res3$gene_id,
                    OrgDb         = org.Hs.eg.db,
                    keyType       = "ENSEMBL",
                    ont           = ont, # "MF", "BP", and "CC", "ALL" (?)
                    pAdjustMethod = "BH",
                    pvalueCutoff  = 1,
                    qvalueCutoff  = 1,
                    readable      = TRUE,
                    minGSSize     = 10,
                    maxGSSize     = 30000)
 
    View(ego@result)
    ego.s<-simplify(ego, cutoff = SIMILARITY_CUTOFF, by = "p.adjust", select_fun = min, measure = "Wang", semData = NULL)
    View(ego.s@result)
    
    tryCatch({
      write.xlsx(ego@result, file = paste0(OUTPUT_DIR,"/","GO_over-representation_",ont,".xlsx",sep=""), sheetName="Sheet1", col.names = T, row.names = F)
      write.xlsx(ego.s@result, file = paste0(OUTPUT_DIR,"/","GO_over-representation_simplified_",ont,".xlsx",sep=""), sheetName="Sheet1", col.names = T, row.names = F)
    }, error = function(e){})
    
    # Preparing data for plotting
    logfc <- as.numeric(genes$log2FoldChange)
    names(logfc) <- genes$gene_id
    
    pdf(file=paste0(OUTPUT_DIR,"/","GO_over-representation_",ont,".pdf",sep=""),width = 10, height = 10)
    print(barplot(ego, showCategory = 50, title = paste0("GO enrichment: ",ont," for ",REGUL,"-regulated genes",sep="")))
    print(dotplot(ego, showCategory = 5, color = "pvalue", title = paste0("GO enrichment: ",ont," for ",REGUL,"-regulated genes",sep="")))
    dev.off()
    #enrichplot::cnetplot(ego, showCategory = 5, foldChange = genes$log2FoldChange, colorEdge = TRUE, categorySize="geneNum")
    
    pdf(file=paste0(OUTPUT_DIR,"/","GO_over-representation_heatplot_",ont,".pdf",sep=""), width = 90)
    tryCatch({
      print(heatplot(ego, showCategory=50, foldChange = logfc))}, error=function(e){})
    dev.off()
  }
}

# fGSEA analysis ####-----------------------------------------------------------

## filter genes
deseq2_tab <- res2
deseq2_tab$X <- rownames(deseq2_tab)

## select just entrez id and logFC
genes <- deseq2_tab[,c("X","stat")]
nrow(genes)
## remove NA values
genes <- na.omit(genes)
nrow(genes)
genes <- genes[order(-genes$stat),]

## change into named vector
rankGenes <- genes$stat
names(rankGenes) <- genes$X
head(rankGenes)

#* Hallmarks ####-----------------
# Prepare Hallmark gene set
packageVersion("msigdbr") #6.1.1

# Read hellmark gene sets for mouse 
h_gene_sets = msigdbr(species = "Mus musculus", category = "H")
msigdbr_t2g = h_gene_sets %>% dplyr::select(gs_name, ensembl_gene) %>% as.data.frame()

fgseaRes <- fgsea(as.list(unstack(rev(msigdbr_t2g))),rankGenes,minSize=15,maxSize=500,eps=0)

tmp <- as.data.frame(fgseaRes)
tmp <- try(transform(tmp, leadingEdge_geneNames = apply(tmp, 1, function(x) toString(bitr(x[8][[1]], fromType = "ENSEMBL", toType = "SYMBOL", OrgDb = "org.Mm.eg.db",)$SYMBOL))))
tmp <- tmp[order(tmp$padj),]
View(tmp)

write.xlsx(tmp, file=paste0(resultdir,"/", cond1,"VS",cond2, "/fGSEA_results_Hallmarks.xlsx"))

# Plot enrichment plots
# take results of fGSEA (from Monika) and re-do what necessary to make enrichment plots
for(i in unique(msigdbr_t2g$gs_name)){
  svg(filename = paste0(resultdir,"/", cond1,"VS",cond2, "/enrichment_plots/Hallmarks/",i,"_enrichPlot.svg",sep=""),width = 8.5, height = 5)
  print(plotEnrichment(as.list(unstack(rev(msigdbr_t2g)))[[i]],
                       rankGenes, gseaParam = 1) + labs(title=i))
  dev.off()
}

topPathwaysUp <- fgseaRes[fgseaRes$NES > 0 &fgseaRes$padj < 0.05,]
topPathwaysDown <- fgseaRes[fgseaRes$NES < 0 &fgseaRes$padj < 0.05,]
topPathwaysDown <- topPathwaysDown[order(topPathwaysDown$padj)]
topPathways <- c(topPathwaysUp$pathway, rev(topPathwaysDown$pathway))
pdf(paste0(resultdir,"/", cond1,"VS",cond2, "/enrichment_plots/Hallmarks/top_enrichment_plots.pdf"),width = 8.5, height = 5)
print(plotGseaTable(as.list(unstack(rev(msigdbr_t2g)))[topPathways], rankGenes, fgseaRes, 
              gseaParam=0.5,colwidths = c(5.4,2,0.6,1,1)))
dev.off()

#* Reactome ####-----------------
# Read Reactome gene sets for mouse 
rctm_gene_sets = msigdbr(species = "Mus musculus", category = "C2", subcategory = "REACTOME")
msigdbr_t2g = rctm_gene_sets %>% dplyr::select(gs_name, ensembl_gene) %>% as.data.frame()

fgseaRes <- fgsea(as.list(unstack(rev(msigdbr_t2g))),rankGenes,minSize=15,maxSize=500,eps=0)

tmp <- as.data.frame(fgseaRes)
tmp <- try(transform(tmp, leadingEdge_geneNames = apply(tmp, 1, function(x) toString(bitr(x[8][[1]], fromType = "ENSEMBL", toType = "SYMBOL", OrgDb = "org.Mm.eg.db",)$SYMBOL))))
tmp <- tmp[order(tmp$padj),]

write.xlsx(tmp, file=paste0(resultdir,"/", cond1,"VS",cond2, "/fGSEA_results_Reactome.xlsx"))

# Plot enrichment plots
# take results of fGSEA (from Monika) and re-do what necessary to make enrichment plots
for(i in unique(fgseaRes$pathway)){
  print(i)
  svg(filename = paste0(resultdir,"/", cond1,"VS",cond2, "/enrichment_plots/Reactome/",i,"_enrichPlot.svg",sep=""),width = 8.5, height = 5)
  print(plotEnrichment(as.list(unstack(rev(msigdbr_t2g)))[[i]],
                       rankGenes, gseaParam = 1) + labs(title=i))
  dev.off()
}

topPathwaysUp <- fgseaRes[fgseaRes$NES > 0 &fgseaRes$padj < 0.01,]
topPathwaysUp <- topPathwaysUp[order(topPathwaysUp$padj)][1:10,]
topPathwaysDown <- fgseaRes[fgseaRes$NES < 0 &fgseaRes$padj < 0.01,]
topPathwaysDown <- topPathwaysDown[order(topPathwaysDown$padj)][1:10,]
topPathways <- c(topPathwaysUp$pathway, rev(topPathwaysDown$pathway))
pdf(paste0(resultdir,"/", cond1,"VS",cond2, "/enrichment_plots/Reactome/top_enrichment_plots.pdf"),width = 10, height = 5)
print(plotGseaTable(as.list(unstack(rev(msigdbr_t2g)))[topPathways], rankGenes, fgseaRes, 
                    gseaParam=0.5,colwidths = c(6,1.5,0.5,0.8,0.7)))
dev.off()

